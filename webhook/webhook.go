package webhook

import (
	"encoding/json"
	"github.com/beenotung/goutils/log"
	"github.com/gorilla/mux"
	"gitlab.com/realtime-syncer/realtime-syncer-go/config"
	"gitlab.com/realtime-syncer/realtime-syncer-go/tracker"
	"net/http"
	"strconv"
	"sync"
)

const www_dir = "www/"

func InitWebHook(conf *config.Config, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		log.Info.Println("init config web hook")
		r := mux.NewRouter()
		r.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {
			http.ServeFile(response, request, www_dir+"index.html")
		})
		r.HandleFunc("/api/peers", func(response http.ResponseWriter, request *http.Request) {
			switch request.Method {
			case http.MethodGet:
				bs, err := json.Marshal(conf.Peers)
				if err != nil {
					log.Error.Println("failed to marshal peers into json", err)
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte("failed to marshal into json"))
					break
				}
				response.Write(bs)
			default:
				response.WriteHeader(http.StatusNotImplemented)
				response.Write([]byte("this request method is not impl"))
			}
		})
		r.HandleFunc("/api/peers/{addr}", func(response http.ResponseWriter, request *http.Request) {
			addr := mux.Vars(request)["addr"]
			if addr == "" {
				response.WriteHeader(http.StatusBadRequest)
				response.Write([]byte("peers address is missing"))
				return
			}
			switch request.Method {
			case http.MethodDelete:
				log.Info.Println("remove peer", addr)
				c := tracker.RemovePeer(conf, addr)
				response.Write([]byte("removed " + strconv.Itoa(c) + "peer"))
			case http.MethodPut:
				log.Info.Println("add peer", addr)
				tracker.AddPeer(conf, addr)
				response.Write([]byte("added peer " + addr))
			default:
				response.WriteHeader(http.StatusNotImplemented)
				response.Write([]byte("this request method is not impl"))
			}
		})
		r.HandleFunc("/api/rootpath", func(response http.ResponseWriter, request *http.Request) {
			switch request.Method {
			case http.MethodGet:
				response.Write([]byte(conf.RootPath))
			case http.MethodPut:
				rootpath := request.FormValue("rootpath")
				if rootpath == "" {
					response.WriteHeader(http.StatusBadRequest)
					response.Write([]byte("rootpath is missing"))
					break
				}
				log.Info.Println("set root path", rootpath)
				// stop all active file tracker
				conf.RootPath = rootpath
				for _, realpath := range tracker.GetFilePaths() {
					tracker.GetFileTracker(realpath).KeepRunning = false
				}
				// start file tracker at new root
				tracker.GetFileTracker(rootpath).Fork(conf, wg)
				response.Write([]byte("updated rootpath : " + rootpath))
			default:
				response.WriteHeader(http.StatusNotImplemented)
				response.Write([]byte("this request method is not impl"))
			}
		})
		log.Info.Println("start web hook on ", conf.WebHookAddr)
		http.ListenAndServe(conf.WebHookAddr, r)
		log.Info.Println("finished init config web hook")
	}()
}
