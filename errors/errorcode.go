package errors

const (
	OK int = iota
	Invalid_Config
	Tracker_Already_Running
	Network_Error
	Failed_String_Process
	File_Read_Error
)
