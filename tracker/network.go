package tracker

import (
	"encoding/json"
	"github.com/beenotung/goutils/log"
	"gitlab.com/realtime-syncer/realtime-syncer-go/config"
	"gitlab.com/realtime-syncer/realtime-syncer-go/errors"
	"gitlab.com/realtime-syncer/realtime-syncer-go/protocol"
	"gitlab.com/realtime-syncer/realtime-syncer-go/utils"
	"net"
	"sync"
)

var encoders_lock = sync.Mutex{}

var encoders = make(map[string]*json.Encoder)

func SendFilePacket(packet protocol.FilePacket) error {
	encoders_lock.Lock()
	defer encoders_lock.Unlock()
	for _, encoder := range encoders {
		err := encoder.Encode(packet)
		if err != nil {
			return err
		}
	}
	return nil
}
func AddPeer(config *config.Config, address string) {
	config.Peers = append(config.Peers, address)
	log.Info.Println("added peer address", address)
	for _, realPath := range GetFilePaths() {
		fi, err := utils.GetFileInfo(realPath)
		if err != nil {
			log.Error.Println("failed get file info")
			continue
		}
		if fi.IsDir() {
			continue
			// skip directory
		}
		// this is a file
		filepacket, errorcode := GetFileTracker(realPath).ToPacket(config)
		if errorcode != errors.OK {
			log.Error.Println("failed to make file packet")
		} else {
			SendFilePacket(filepacket)
		}
	}
}
func RemovePeer(conf *config.Config, addr string) (count int) {
	count = 0
	for i := len(conf.Peers) - 1; i >= 0; i-- {
		if conf.Peers[i] == addr {
			conf.Peers = append(conf.Peers[:i], conf.Peers[i+1:]...)
			count++
		}
	}
	encoders_lock.Lock()
	delete(encoders, addr)
	encoders_lock.Unlock()
	return count
}
func handleConn(config *config.Config, conn net.Conn, wg *sync.WaitGroup, isFromClient bool) {
	defer func() {
		encoders_lock.Lock()
		defer encoders_lock.Unlock()
		delete(encoders, conn.LocalAddr().String())
		conn.Close()
		wg.Done()
	}()
	log.Info.Println("connected to", conn.RemoteAddr().String())
	decoder := json.NewDecoder(conn)
	encoder := json.NewEncoder(conn)
	encoders[conn.LocalAddr().String()] = encoder
	if !isFromClient {
		// send self address to peer in case reconnection is needed (e.g. restart)
		log.Debug.Println("try to send self addr")
		_, port, err := net.SplitHostPort(config.SelfAddr)
		if err != nil {
			log.Error.Println("failed to parse port")
		} else {
			err = encoder.Encode(protocol.NewServerAddress(port))
			if err != nil {
				log.Error.Println("failed to send self addr", err)
			}
		}
	}
loop:
	for {
		decoder.UseNumber()
		packet := make(map[string]interface{})
		err := decoder.Decode(&packet)
		if err != nil {
			break
		}
		switch packet[protocol.PacketType_s].(type) {
		case json.Number:
			packetType, err := packet[protocol.PacketType_s].(json.Number).Int64()
			if err != nil {
				log.Error.Println("failed to parse packet type", err)
				continue loop
			}
			//log.Info.Println("received packet type", packetType)
			switch packetType {
			case protocol.FilePacket_t:
				log.Debug.Println("received file packet")
				p := protocol.FilePacket{}
				bs, err := json.Marshal(packet)
				if err != nil {
					log.Error.Println("failed to marshal packet", err)
					continue loop
				}
				err = json.Unmarshal(bs, &p)
				if err != nil {
					log.Error.Println("failed to unmarshal packet", err)
					continue loop
				}
				realpath := utils.VirtualPathToRealPath(p.VirtualPath, config.RootPath)
				tracker := *GetFileTracker(realpath)
				isLocalNewer := tracker.CompareAndSet(p)
				if isLocalNewer {
					log.Error.Println("not impl")
					packet, errorcode := protocol.NewFilePacket(*config, p.LastSyncTime, p.Hash, realpath, p.Content)
					if errorcode != errors.OK {
						log.Error.Println("failed to prepare file packet")
						break
					}
					SendFilePacket(packet)
				}
			case protocol.ServerAddress_t:
				//log.Debug.Println("received server addr", packet)
				p := protocol.ServerAddress{}
				bs, err := json.Marshal(packet)
				if err != nil {
					log.Error.Println("failed to marshal packet", err)
					continue loop
				}
				err = json.Unmarshal(bs, &p)
				if err != nil {
					log.Error.Println("failed to unmarshal packet", err)
					continue loop
				}
				address := conn.RemoteAddr().String()
				host, _, err := net.SplitHostPort(address)
				if err != nil {
					log.Error.Println("failed to parse host")
				} else {
					address = host + ":" + p.Port
					AddPeer(config, address)
				}
			default:
				log.Debug.Println("received unknown packet", packet)
			}
		default:
			log.Error.Println("received packet wrong format", packet)
		}
	}
}
func InitNetwork(config *config.Config, wg *sync.WaitGroup) error {
	log.Info.Println("event handler start init")
	ln, err := net.Listen("tcp", config.SelfAddr)
	if err != nil {
		log.Error.Println(err)
		return err
	}
	// init listener
	wg.Add(1)
	go func() {
		defer func() {
			ln.Close()
			wg.Done()
		}()
		for {
			conn, err := ln.Accept()
			if err != nil {
				break
			}
			wg.Add(1)
			go handleConn(config, conn, wg, true)
		}
	}()
	// connect to peer
	for _, peer := range config.Peers {
		conn, err := net.Dial("tcp", peer)
		if err != nil {
			log.Error.Println("failed to connect to peer", err)
			continue
		}
		wg.Add(1)
		go handleConn(config, conn, wg, false)
	}
	log.Info.Println("event handler finished init")
	return nil
}
