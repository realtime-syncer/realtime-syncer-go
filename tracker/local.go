package tracker

import (
	"crypto/md5"
	"github.com/beenotung/goutils/log"
	"gitlab.com/realtime-syncer/realtime-syncer-go/config"
	"gitlab.com/realtime-syncer/realtime-syncer-go/errors"
	"gitlab.com/realtime-syncer/realtime-syncer-go/protocol"
	"gitlab.com/realtime-syncer/realtime-syncer-go/utils"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"sync"
	"time"
)

var (
	ScanInterval = 1000 * time.Millisecond
	dict         = make(map[string]*FileTracker)
	dict_lock    = sync.Mutex{}
)

type FileTracker struct {
	Path         string // real path (pointing to local file system)
	KeepRunning  bool
	isRunning    bool
	lock         sync.Mutex
	hash         []byte
	lastSyncTime int64
}

/* real path (pointing to local file system) */
func GetFileTracker(path string) *FileTracker {
	//Debug.Println("get File Tracker", path)
	dict_lock.Lock()
	defer dict_lock.Unlock()
	if x, b := dict[path]; b {
		//Debug.Println("found")
		return x
	} else {
		x := &FileTracker{
			Path:      path,
			isRunning: false,
		}
		dict[path] = x
		//Debug.Println("created")
		return x
	}
}
func RemoveFileTracker(path string) {
	dict_lock.Lock()
	defer dict_lock.Unlock()
	delete(dict, path)
}

/* real file path string array */
func GetFilePaths() []string {
	dict_lock.Lock()
	defer dict_lock.Unlock()
	ks := make([]string, 0)
	for k := range dict {
		ks = append(ks, k)
	}
	return ks
}
func (p *FileTracker) CompareAndSet(packet protocol.FilePacket) (isLocalNewer bool) {
	if utils.EqualBytes(p.hash, packet.Hash) {
		return false
	}
	if p.lastSyncTime > packet.LastSyncTime {
		return true
	}
	if p.lastSyncTime == packet.LastSyncTime {
		//conflict
		//TODO resolve conflict
		p.updateContent(packet)
		return false
	}
	// local is outdated
	p.updateContent(packet)
	return false
}
func (p *FileTracker) updateContent(packet protocol.FilePacket) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.hash = packet.Hash
	p.lastSyncTime = packet.LastSyncTime
	f, err := os.Create(p.Path)
	if err != nil {
		// try to create parent directory
		dir, _ := filepath.Split(p.Path)
		err2 := os.MkdirAll(dir, os.ModePerm)
		if err2 != nil {
			log.Error.Println("failed to create directory", err2)
			return
		}
		f, err = os.Create(p.Path)
		if err != nil {
			log.Error.Println("failed to write to file", err)
		}
	}
	defer f.Close()
	f.Write(packet.Content)
}
func (p *FileTracker) ToPacket(config *config.Config) (packet protocol.FilePacket, errorcode int) {
	content, err := utils.GetFileContent(p.Path)
	if err != nil {
		log.Error.Println("failed to get file content", err)
		errorcode = errors.File_Read_Error
		return
	}
	packet, errorcode = protocol.NewFilePacket(*config, p.lastSyncTime, p.hash, p.Path, content)
	if errorcode != errors.OK {
		log.Error.Println("failed to prepare file packet")
	}
	return
}
func (p *FileTracker) Fork(config *config.Config, wg *sync.WaitGroup) {
	p.lock.Lock()
	if p.isRunning {
		p.lock.Unlock()
		return
	}
	p.lock.Unlock()
	p.isRunning = true
	wg.Add(1)
	p.KeepRunning = true
	go func() {
		defer func() {
			log.Info.Println("stop tracking", p.Path)
			wg.Done()
		}()
		log.Info.Println("start tracking", p.Path)
		// check if it is folder or file
		f, err := os.Open(p.Path)
		if err != nil {
			log.Error.Println("failed to open", err)
			return
		}
		fi, err := f.Stat()
		f.Close()
		if err != nil {
			log.Error.Println("failed to get file stat", err)
			return
		}
		log.Debug.Println("start local file loop")
		for p.KeepRunning {
			switch mode := fi.Mode(); {
			case mode.IsDir():
				//log.Info.Println("this is directory")
				fis, err := ioutil.ReadDir(p.Path)
				if err != nil {
					log.Error.Println("failed to read directory", err)
					return
				}
				for _, fi := range fis {
					x := GetFileTracker(path.Join(p.Path, fi.Name()))
					x.Fork(config, wg)
				}
			case mode.IsRegular():
				//log.Info.Println("this is file")
				//TODO avoid checking when updating, use waitgroup
				//p.lock.Lock()
				//defer p.lock.Unlock()
				content, err := utils.GetFileContent(p.Path)
				if err != nil {
					log.Error.Println("failed to get file content", err)
					// maybe the file is deleted locally
					p.KeepRunning = false
					RemoveFileTracker(p.Path)
					break
				}
				hash := md5.New().Sum(content)
				if !utils.EqualBytes(hash, p.hash) {
					log.Info.Println("file change detected on", p.Path)
					p.lastSyncTime = time.Now().UnixNano()
					p.hash = hash
					packet, errorcode := protocol.NewFilePacket(*config, p.lastSyncTime, p.hash, p.Path, content)
					if errorcode != errors.OK {
						log.Error.Println("failed to prepare file packet")
						break
					}
					SendFilePacket(packet)
				}
			default:
				log.Error.Println("unknown mode", mode)
				return
			}
			if p.KeepRunning {
				//log.Debug.Println("before sleep")
				time.Sleep(ScanInterval)
				//log.Debug.Println("after sleep")
			}
		}
		log.Debug.Println("stop local file loop")
		p.isRunning = false
		dict_lock.Lock()
		defer dict_lock.Unlock()
		delete(dict, p.Path)
	}()
}
