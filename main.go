package main

import (
	"fmt"
	. "github.com/beenotung/goutils/log"
	"gitlab.com/realtime-syncer/realtime-syncer-go/config"
	"gitlab.com/realtime-syncer/realtime-syncer-go/errors"
	"gitlab.com/realtime-syncer/realtime-syncer-go/tracker"
	"gitlab.com/realtime-syncer/realtime-syncer-go/webhook"
	"os"
	"sync"
)

func init() {
	Init(true, true, true, ShortCommFlag)
}
func main() {
	Info.Println("main start")
	configPath := "config.json" // default config path
	if len(os.Args) == 2 {
		configPath = os.Args[1]
	}
	conf, err := config.LoadConfig(configPath)
	if err != nil {
		Error.Println("failed to load config", err)
		os.Exit(errors.Invalid_Config)
	}
	wg := &sync.WaitGroup{}
	webhook.InitWebHook(&conf, wg)
	err = tracker.InitNetwork(&conf, wg)
	fmt.Println("************************************************")
	fmt.Println("*                                               ")
	fmt.Println("* Web Config Address : " + conf.WebHookAddr)
	fmt.Println("*                                               ")
	fmt.Println("* File Packet Address : " + conf.SelfAddr)
	fmt.Println("*                                               ")
	fmt.Println("************************************************")
	if err != nil {
		Error.Println("failed to init network", err)
		os.Exit(errors.Network_Error)
	}
	tracker.GetFileTracker(conf.RootPath).Fork(&conf, wg)
	wg.Wait()
	Info.Println("main end")
}
