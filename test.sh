#!/bin/bash
# usage example:
#   ./test.sh 1 #robotA
#   ./test.sh 2 #controllerA
mkdir -p /tmp/robotA
mkdir -p /tmp/controllerA
case $1 in
[1]*)
  go run main.go res_test/config_robotA.json
  ;;
[2]*)
  go run main.go res_test/config_controllerA.json
  ;;
esac