package utils

import (
	"github.com/beenotung/goutils/log"
	"gitlab.com/realtime-syncer/realtime-syncer-go/errors"
	"io/ioutil"
	"os"
	"strings"
)

func RealPathToVirtualPath(realpath string, rootpath string) (virtualpath string, errorcode int) {
	i := strings.Index(realpath, rootpath)
	if i == -1 {
		log.Error.Println("failede to convert path", "rootpath:", rootpath, "realpath:", realpath)
		errorcode = errors.Failed_String_Process
		return
	}
	virtualpath = realpath[len(rootpath):]
	errorcode = errors.OK
	return
}

//TODO to support multiple folder mapping under virtualpath
func VirtualPathToRealPath(virtualpath string, rootpath string) string {
	return rootpath + virtualpath
}
func EqualBytes(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
func GetFileContent(path string) (content []byte, err error) {
	var f *os.File
	f, err = os.Open(path)
	if err != nil {
		log.Error.Println("failed to open file", err)
		return
	}
	defer f.Close()
	content, err = ioutil.ReadAll(f)
	if err != nil {
		log.Error.Println("failed to read file content", err)
	}
	return
}
func GetFileInfo(path string) (os.FileInfo, error) {
	f, err := os.Open(path)
	if err != nil {
		log.Error.Println("failed to open file", err)
		return nil, err
	}
	defer f.Close()
	return f.Stat()
}
