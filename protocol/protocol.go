package protocol

import (
	"gitlab.com/realtime-syncer/realtime-syncer-go/config"
	"gitlab.com/realtime-syncer/realtime-syncer-go/errors"
	"gitlab.com/realtime-syncer/realtime-syncer-go/utils"
)

const PacketType_s = "PacketType"
const (
	FilePacket_t = 1 + iota
	ServerAddress_t
)

type FilePacket struct {
	PacketType   int
	LastSyncTime int64
	Hash         []byte
	VirtualPath  string
	Content      []byte
}

func NewFilePacket(config config.Config, lastSyncTime int64, hash []byte, realpath string, content []byte) (packet FilePacket, errorcode int) {
	var virtualpath string
	virtualpath, errorcode = utils.RealPathToVirtualPath(realpath, config.RootPath)
	if errorcode != errors.OK {
		return
	}
	return FilePacket{FilePacket_t, lastSyncTime, hash, virtualpath, content}, errors.OK
}

type ServerAddress struct {
	PacketType int
	//Address    string
	Port string
}

func NewServerAddress(port string) ServerAddress {
	return ServerAddress{ServerAddress_t, port}
}
