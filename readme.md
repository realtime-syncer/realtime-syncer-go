# Real Time Syncer (Golang)
sync files between robots for easy-to-use communication purpose

## Goal
1. sync file
2. easy to set-up
3. fast runtime performance
4. extensible (maybe)

## Road Map
 - [x] file* scanning
 - [ ] file* adding (maybe drag folder)
   - multiple root ?
 - [ ] file* syncing (networking)
 - [ ] file* visualize (UI)
   - show tree struc in web front-end ?
 * remark file refer directory or really file

## Idea
 - file scanning
   - [x] scan recursively
   - [x] time interval
   - [x] save last change time
 - UI
   - config file
   - command line
   - web

# Main Flow
1. load from config
2. loop
   1. add new file
      1. accept new file/folder (from UI)
      2. scan recursively
      3. register to syncer
   2. sync files
      1. wait peer message
         1. update local files
      2. sent to peer if detected local change
3. save config

# Notes
1. Network Protocol
This application is using TCP for the following reason
   1. to support large file
      - maybe implement segmentation in the future
   2. reliability
      - in case the file doesn't change frequently, active syncing will be needed
