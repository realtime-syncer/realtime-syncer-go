package config

import (
	"encoding/json"
	"errors"
	. "github.com/beenotung/goutils/log"
	"os"
)

type Config struct {
	RootPath    string
	SelfAddr    string
	WebHookAddr string
	Peers       []string
}

func LoadConfig(filename string) (config Config, err error) {
	Debug.Println("loading config from", filename)
	var file *os.File
	file, err = os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()
	err = json.NewDecoder(file).Decode(&config)
	if err != nil {
		return
	}
	if len(config.RootPath) == 0 {
		err = errors.New("config file missing RootPath!")
		return
	}
	return
}
